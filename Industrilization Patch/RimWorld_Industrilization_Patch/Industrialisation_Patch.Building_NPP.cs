﻿using RimWorld;
using System;
using Verse;

namespace Industrialisation_Patch
{
    public static class ThingComp_Extensions
    {
        public static Industrialisation_Patch.CompPropertiesMin CompPropertiesMin(this ThingComp thingComp)
        {
            return thingComp.props as Industrialisation_Patch.CompPropertiesMin;
        }
    }

    public class CompPropertiesMin : CompProperties
    {
        public float heatPushMinTemperature = -9999;

        public CompPropertiesMin()
        {
            compClass = typeof(CompPropertiesMin);
        }
    }

    public class MinCompHeatPusherPowered : ThingComp
    {
        CompPropertiesMin compPropertiesMin;
        CompPowerTrader compPowerTrader;

        public override void PostSpawnSetup()
        {
            base.PostSpawnSetup();
            compPropertiesMin = this.CompPropertiesMin();
            compPowerTrader = (this.parent as Building).PowerComp as CompPowerTrader;
        }

        public override void CompTick()
        {
            base.CompTick();
            if (!Gen.IsHashIntervalTick((Thing)this.parent, 60))
                return;

            float temp = GridsUtility.GetTemperature(this.parent.Position);
            if (compPowerTrader == null || !compPowerTrader.PowerOn || compPowerTrader.PowerNet == null || (double)temp >= (double)this.props.heatPushMaxTemperature || (double)temp <= (double)this.compPropertiesMin.heatPushMinTemperature)
                return;

            GenTemperature.PushHeat(this.parent.Position, this.props.heatPerSecond);

            temp = GridsUtility.GetTemperature(this.parent.Position);
            if ((double)temp >= (double)this.props.heatPushMaxTemperature)
            {
                Room room = this.parent.Position.GetRoom();
                if (room != null)
                { 
                    room.Temperature = this.props.heatPushMaxTemperature;
                }
                else
                {
                    Log.Error("ROOM IS NULL");
                }
            }

            if ((double)temp <= (double)this.compPropertiesMin.heatPushMinTemperature)
            {
                Room room = this.parent.Position.GetRoom();
                if (room != null)
                {
                    room.Temperature = this.compPropertiesMin.heatPushMinTemperature;
                }
                else
                {
                    Log.Error("ROOM IS NULL");
                }
            }

        }
    }

    public class Building_NPP : Building_PowerPlantSteam
    {
        private int Burnticks = 40;
        private int damageSelfCounterStart = 600;
        private Random random = new Random();
        private int damageAppliedSelfMin;
        private int damageAppliedSelfMax;
        private int damageAppliedSelf;
        private int doDamageSelfCounter;
        private int overheartThreshold;
        private Room myRoom;

        public override void SpawnSetup()
        {
            base.SpawnSetup();
            this.SetWorkVariables();
        }

        public override void PostMake()
        {
            base.PostMake();
        }

        public override void ExposeData()
        {
            base.ExposeData();
            this.SetWorkVariables();
        }

        private void SetWorkVariables()
        {
            ThingDef_NPP thingDefNpp = (ThingDef_NPP)this.def;
            this.damageAppliedSelfMin = thingDefNpp.DamageAppliedSelfMin;
            this.damageAppliedSelfMax = thingDefNpp.DamageAppliedSelfMax;
            this.damageSelfCounterStart = thingDefNpp.DamageApplySelfCounter;
            this.overheartThreshold = thingDefNpp.OverheatThreshold;

            myRoom = this.Position.GetRoom();
        }

        public override void Tick()
        {
            base.Tick();
            int num = this.Burnticks - 1;
            this.Burnticks = num;
            if (num == 0)
            {
                IntVec3 position = this.Position;
                MoteThrower.ThrowSmoke(position.ToVector3Shifted(), 3f);
                position = this.Position;
                MoteThrower.ThrowSmoke(position.ToVector3Shifted(), 4f);
                this.Burnticks = 40;
            }
            this.doDamageSelfCounter = this.doDamageSelfCounter - 1;
            if (this.doDamageSelfCounter > 0)
                return;
            this.doDamageSelfCounter = this.damageSelfCounterStart;
            this.DoDamageSelf();
        }

        private void DoDamageSelf()
        {
            this.damageAppliedSelf = this.random.Next(this.damageAppliedSelfMin, this.damageAppliedSelfMax);
            if ((this.damageAppliedSelf == 0 || this.HitPoints > this.overheartThreshold) && (GridsUtility.GetTemperature(this.Position) < 250))
                return;
            this.TakeDamage(new DamageInfo(DamageDefOf.Flame, this.damageAppliedSelf, (Thing)this, new BodyPartDamageInfo?(), (ThingDef)null));
            GenTemperature.PushHeat(this.Position, 200);
            Messages.Message(Translator.Translate("NuclearMeltdownStart"), MessageSound.SeriousAlert);
        }
    }

    public class Building_NPP_Cooler : Building_PowerPlantSteam
    {
        private int damageSelfCounterStart = 600;
        private Random random = new Random();
        private int damageAppliedSelfMin;
        private int damageAppliedSelfMax;
        private int damageAppliedSelf;
        private int doDamageSelfCounter;
        private int overheartThreshold;
        private Room myRoom;

        public override void SpawnSetup()
        {
            base.SpawnSetup();
            this.SetWorkVariables();
        }

        public override void PostMake()
        {
            base.PostMake();
        }

        public override void ExposeData()
        {
            base.ExposeData();
            this.SetWorkVariables();
        }

        private void SetWorkVariables()
        {
            ThingDef_NPP_Cooler thingDefNpp = (ThingDef_NPP_Cooler)this.def;
            this.damageAppliedSelfMin = thingDefNpp.DamageAppliedSelfMin;
            this.damageAppliedSelfMax = thingDefNpp.DamageAppliedSelfMax;
            this.damageSelfCounterStart = thingDefNpp.DamageApplySelfCounter;
            this.overheartThreshold = thingDefNpp.OverheatThreshold;

            myRoom = this.Position.GetRoom();
        }

        public override void Tick()
        {
            base.Tick();
            this.doDamageSelfCounter = this.doDamageSelfCounter - 1;
            if (this.doDamageSelfCounter > 0)
                return;
            this.doDamageSelfCounter = this.damageSelfCounterStart;
            this.DoDamageSelf();
        }

        private void DoDamageSelf()
        {
            this.damageAppliedSelf = this.random.Next(this.damageAppliedSelfMin, this.damageAppliedSelfMax);
            if ((this.damageAppliedSelf == 0 || this.HitPoints > this.overheartThreshold) && (GridsUtility.GetTemperature(this.Position) < 400))
                return;
            this.TakeDamage(new DamageInfo(DamageDefOf.Flame, this.damageAppliedSelf, (Thing)this, new BodyPartDamageInfo?(), (ThingDef)null));
            GenTemperature.PushHeat(this.Position, 50);
            Messages.Message(Translator.Translate("NuclearMeltdownStart"), MessageSound.SeriousAlert);
        }
    }
}