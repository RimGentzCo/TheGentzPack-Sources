﻿using Verse;

namespace Industrialisation_Patch
{
    public class ThingDef_NPP : ThingDef
    {
        public int DamageAppliedSelfMin;
        public int DamageAppliedSelfMax;
        public int DamageApplySelfCounter;
        public int OverheatThreshold;
    }
}

namespace Industrialisation_Patch
{
    public class ThingDef_NPP_Cooler : ThingDef
    {
        public int DamageAppliedSelfMin;
        public int DamageAppliedSelfMax;
        public int DamageApplySelfCounter;
        public int OverheatThreshold;
    }
}