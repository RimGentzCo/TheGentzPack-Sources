﻿using System.Linq;
using System.Text;
using UnityEngine;

using RimWorld;
using Verse;
using CommunityCoreLibrary;

namespace NoStone
{
    public class NoStone : SpecialInjector
    {
        public override void Inject()
        {
            var thingDef = ThingDef.Named("TableStonecutter");
            var recipeDef = DefDatabase<RecipeDef>.GetNamed("MakeStoneBlocks");
            thingDef.recipes.Remove(recipeDef);
            thingDef.RecacheRecipes(false);
        }

    }
}
